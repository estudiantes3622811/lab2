from pages.login_m import LoginPage
from selenium import webdriver
from selenium.webdriver.common.by import By
from core.driver_m import DriverFactory


class TestSuite:

    def test_001_navigate_to_url(self):
        browser = DriverFactory.getBrowser("chrome") # el nombre del browser viene de afuera
        browser.get("https://www.saucedemo.com/v1")
        login_page = LoginPage()
        login_page.init_elements(browser)
        login_page.execute_login("123", "456")
        msg = login_page.get_error_message()
        assert msg ==  "Epic sadface: Username and password do not match any user in this service"

    def test_002_navigate_s(self):
        browser = DriverFactory.getBrowser("firefox")
        browser.get("https://www.saucedemo.com/v1")
        
        login_page = LoginPage()
        login_page.init_elements(browser)
        login_page.execute_login("123", "456")
        msg = login_page.get_error_message()
        
        msg = browser.find_element(By.CSS_SELECTOR, "h3[data-test=error]").text
        assert msg ==  "Epic sadface: Username and password do not match any user in this service"
