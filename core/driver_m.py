from selenium import webdriver


class DriverFactory:
    @staticmethod
    def getBrowser(browser_name):
        if(browser_name == "chrome"):
            return webdriver.Chrome()
        elif(browser_name == "firefox"):
            return webdriver.Firefox()