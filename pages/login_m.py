from selenium.webdriver.common.by import By

class LoginPage:
        input_username = None
        input_pass = None
        button_login = None
        error_msg = None
        browser = None

        def init_elements(self, browser):
            self.browser = browser
            self.input_username = browser.find_element(By.CSS_SELECTOR, "input[id='user-name']")
            self.input_pass = browser.find_element(By.CSS_SELECTOR, "input[id='password']")
            self.button_login = browser.find_element(By.CSS_SELECTOR, "input[id='login-button']")
        
        def execute_login(self, username, password):
            self.input_username.send_keys(username)
            self.input_pass.send_keys(password)
            self.button_login.click()
        
        def get_error_message(self):
             self.error_msg = self.browser.find_element(By.CSS_SELECTOR, "h3[data-test=error]")
             return self.error_msg.text